# Nixie Clock
A Nixie clock with dimmable lamps.  Controlled by an arduino and Raspberry Pi.


## Datasheets
Datasheets for nearly every component used in this project can be found in the datasheets directory.  

Obviously I take NO credit for these documents, they are only provided as a reference.


## DC Power Estimates

### 170V Line
| Device           | Voltage | Current Draw (Amps) | Count | 
| -------          | :-----: |:-------------------:| :---: |
| IN-14 Nixie Tube | 170     | 0.0025              | 6     |
| Total            | -       | 0.015               | -     |
170V power = 2.55 W

### 12V Line
| Device  | Voltage        | Current Draw (Amps) | Count | 
| ------- | :-----------:  |:-------------------:| :---: |
| LED     | 12             | ~0.33 (MAX)         | 3     |
| Total   | -              | 1.00                | -     |
12V power = 12 W


### 5V Line
| Device       | Voltage        | Current Draw (Amps) | Count | 
| -------      | :-----------:  |:-------------------:| :---: |
| Arduino      | 12             | 0.02                | 1     |
| Raspberry Pi | 5              | 0.95                | 1     |
| PCA9685      | 5              | 0.01                | 2     |
| Total        | -              | 0.99                | -     |
5V power = 4.95 W

### *Total DC Power is* `19.50 Watts`



Nixie Power estimates taken from it's datasheet.

Raspberry Pi power estimates can be found [here.](https://www.pidramble.com/wiki/benchmarks/power-consumption)

Arduino power estimates found [here.](https://www.ba0sh1.com/blog/2012/05/24/project-crystal-part-1/)

LED power estimates derived from wattage.

PCA9685 power estimates taken from page 38 of it's Data sheet.

## General Circuit Diagram
![alt text](media/circuit_diagram.png)

### General Description:
The Clock takes 120 VAC-RMS as it's only power source.  The 120 V AC is then transformed down to 12VRMS and rectified.  From here the rectified power is then filtered using capacitors and inductors. This 12 VDC is then converted to 170 V (for the nixies) as well as 5V (for the logic devices).

The 120 VAC is also used to power a lamp.  This lamp is dimmable, so a mosfet and power detection circuit is used.  This is explained in more detail below.


## Nixie Tubes:

* The nixies are controlled with the K155ID IC. 
* An annode resistor is used to ensure that the nixie remains at a safe current level during operation. 
* The 170V power supply is able to be turned off with an N-channel mosfet.  This provides some power savings, in the event that the tubes are turned off (say at night), and some added safety (This way we don't have 170 VDC as soon as the clock is plugged in!).  

* The tubes are dimmable, this is achieved with a P-channel mosfet attached on the positive side of the power supply.  The tubes can then be dimmed with PWM.  Due to the P-channel mosfet needing to see a negative gate-source voltage in order to conduct; an opticoupler is used to avoid exposing any logic circuits to 170V.  

## Dimmable Lamps

* These lamps are just regular AC lights.  They are dimmed with an N-channel mosfet.  Switching an AC circuit with a mosfet is not normally possible; However a recifier is used in series with the light to allow the mosfet to switch the circuit.  A photovoltaic mosfet driver is used to avoid having to tie the AC neutral line and the DC GND together.  Otherwise we could not guarantee that the +5V applied to the mosfet gate would actually be +5V when the mosfet source is referenced as ground (as the AC and DC circuits are isolated due to the transformer).  

* A multitude of modes are planned for this, including leading-edge, trailing-edge, and PWM.  In order to do these dimming methods effectively, we need to know when the AC wave goes from negative to positive (and vice-versa).  For this an optocoupler is used to sense when the AC wave begins.



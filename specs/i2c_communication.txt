General format:

| 0   | 1-31 |
| reg | data | 

reads are specified by master writing only a reg, then reading resp.

writes done by writing reg:data OR using the 0x01 reg for extended data.

Nano - Slave
Pi - Master

 ----------------------------- I2c slave reg map ------------------------------
| ============================================================================ |
|  addr  | data | function                                                     |
| ============================================================================ |
|  0x00  |   y  | reset if data                                                |
|  0x00  |   n  | get status                                                   |
| ---------------------------------------------------------------------------- |
|  0x01  |   y  | extended input register: data[0] = reg, data[1] = data size. |
|        |      | the following transmission(s) will then be treated as raw    |
|        |      | input data for the specified register.  Timeout of 5s        |
| ---------------------------------------------------------------------------- |
|  0x10  |   y  | Set integrated lamp brightness                               |
|  0x10  |   n  | get integrated lamp brightness                               |
| ---------------------------------------------------------------------------- |
|  0x11  |   y  | set external lamp brightness                                 |
|  0x11  |   n  | get external lamp brightness                                 |
| ---------------------------------------------------------------------------- |
|  0x20  |   n  | get pot0 val                                                 |
| ---------------------------------------------------------------------------- |
|  0x21  |   n  | get pot1 val                                                 |
| ---------------------------------------------------------------------------- |
|  0xF0  |   y  | Set integrated lamp dim style: 0 = leading edge,             |
|        |      | 1 = trailing edge, 2 = PWM                                   |
| ============================================================================ |

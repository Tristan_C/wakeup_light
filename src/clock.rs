use pwm_pca9685 as pca9685;
use pca9685::{Channel, Pca9685, SlaveAddr };
use crate::PWM_DRIVERS;
use std::thread;
use std::sync::{mpsc, Mutex, Arc};
use std::time::{Duration, SystemTime};
use std::process;
use chrono::{DateTime, Timelike};
use std::convert::{TryFrom, TryInto};


struct ClockData{
    channels_map: [[usize; 4]; 6],
    nixie_pwm_pin: usize,
    blank: bool,
    brightness: u8,
    brightness_off_count: u16,
    display_time: bool,
    clock_thread: Option<thread::JoinHandle<()>>,
    clock_thread_tx: Option<std::sync::mpsc::Sender<i32>>,
}


pub struct Clock{
    data: Arc<Mutex<ClockData>>,

}



impl Clock{
    pub fn new() -> Self {
        let dta = ClockData{
            channels_map: [[0,1,2,3], [4,5,6,7], [8,9,10,11], 
                [12,13,14,15], [16,17,18,19], [20,21,22,23]],
            
            nixie_pwm_pin: 24,
            blank: true,
            brightness: 100 as u8,
            brightness_off_count: 0 as u16,
            display_time: true,
            clock_thread: None,
            clock_thread_tx: None,

        };

        return Clock{data: Arc::new(Mutex::new(dta))};
    }




    pub fn init_display(&self){

        let mut pwm;
        println!("init_disp");
        
        // Set all digits full on.  (rest of methods assume this has been set!)
        for digit in self.data.lock().unwrap().channels_map.iter(){
            for channel in digit.iter(){
                println!("setting up Channel {}...", channel);
                if *channel > 15{
                    pwm = PWM_DRIVERS[1].lock().unwrap();
                }
                else{
                    pwm = PWM_DRIVERS[0].lock().unwrap();
                }

                pwm.set_channel_full_on(
                    Channel::try_from((*channel) % 16).unwrap(), 0).unwrap();
                drop(pwm);
            }
        }

        self.display([0,0,0,0,0,0]);
        println!("init set display");
        // Pass set_brightness the current brightness value, as we are assuming
        // that the brightness off_count var is garbage.  Also has the effect
        // of unblanking the display.
        

        self.set_brightness(100u8);
        println!("init set brightness");
    }


    pub fn blank_output(&self){
        let mut pwm = PWM_DRIVERS[0].lock().unwrap();

        let mut dta = self.data.lock().unwrap();
        let mut pwm_pin = dta.nixie_pwm_pin;

        if pwm_pin > 15{
            drop(pwm);
            pwm = PWM_DRIVERS[1].lock().unwrap();
        }

        pwm.set_channel_full_off(Channel::try_from(pwm_pin % 16).unwrap())
            .unwrap();
        dta.blank = true;
    }


    pub fn unblank_output(&self){
        let mut pwm = PWM_DRIVERS[0].lock().unwrap();

        let mut dta = self.data.lock().unwrap();
        let pwm_pin = dta.nixie_pwm_pin;
        let brightness_off_count = dta.brightness_off_count;

        if pwm_pin > 15{
            drop(pwm);
            pwm = PWM_DRIVERS[1].lock().unwrap();
        }

        pwm.set_channel_off(Channel::try_from(pwm_pin % 16).unwrap(), 
                            brightness_off_count).unwrap();

        pwm.set_channel_on(Channel::try_from(pwm_pin % 16).unwrap(), 
                           0).unwrap();

        dta.blank = false;
    }


    pub fn set_brightness(&self, brightness: u8){
        let mut data = self.data.lock().unwrap();
        data.brightness = brightness % 100;
        data.brightness_off_count = 
            ((data.brightness as f32)/ 100.0 * 4095.0).floor() as u16;
        drop(data);
        self.unblank_output();
    }


    pub fn set_digit(&self, digit: usize, val: u8){
        // Assumes that full_on has been set on all relevant channels.
        let mut pwm;     

        for (i, channel) in self.data.lock().unwrap().channels_map[digit].iter().enumerate(){

            // Decide which pwm controller is referenced
            if *channel > 15{
                pwm = PWM_DRIVERS[1].lock().unwrap();
            }
            else{
                pwm = PWM_DRIVERS[0].lock().unwrap();

            }

            // Set the BCD chips based on val requested.
            if (val >> i) & 0b01 != 0{
                // Unset the Full off precedent. -> reverting to full on.
                pwm.set_channel_off(
                    Channel::try_from(channel % 16).unwrap(), 4095).unwrap();
            } 


            else{
                // Set the Full off precedent
                pwm.set_channel_full_off(
                    Channel::try_from(channel % 16).unwrap()).unwrap();
            }

            drop(pwm);
        }
    }


    pub fn start_clock_thread(&self){
        let (tx, rx) = mpsc::channel::<i32>();
        let new_self = Clock{data: self.data.clone()};
        let mut dta = self.data.lock().unwrap();
        let handle = thread::spawn(|| {new_self.mainloop(rx)});
        dta.clock_thread = Some(handle);
        dta.clock_thread_tx = Some(tx);
    }

    pub fn stop_clock_thread(&self){
        self.data.lock().unwrap().clock_thread_tx.as_ref().unwrap().send(-1).unwrap();
    }


    pub fn stop_time_display(&self){
        self.data.lock().unwrap().display_time = false;
    }


    pub fn start_time_display(&self){
        self.data.lock().unwrap().display_time = true;
    }


    pub fn display_time(&self, hours: u32, mins: u32, secs: u32){
        let mut disp_arr: [u8; 6] = [0; 6];
        disp_arr[0] = (hours / 10).try_into().unwrap();
        disp_arr[1] = (hours % 10).try_into().unwrap();
        disp_arr[2] = (mins / 10).try_into().unwrap();
        disp_arr[3] = (mins % 10).try_into().unwrap();
        disp_arr[4] = (secs / 10).try_into().unwrap();
        disp_arr[5] = (secs % 10).try_into().unwrap();
        println!("disp_arr = {:?}", disp_arr);
        self.display(disp_arr);
    }


    pub fn display(&self, display_arr: [u8; 6]){
        println!("disp");

        self.blank_output();
        for (digit, val) in display_arr.iter().enumerate(){
            self.set_digit(digit, *val);
        }

        self.unblank_output();
    }


    fn recv_sig(&self, sig: i32){
        match sig{
            -1 => process::exit(0),
            s => eprintln!("Clock thread got unknown signal {}", s),
        }
    }


    fn mainloop(self, rx: std::sync::mpsc::Receiver<i32>){
        let mut now: u64;
        let mut sys_now: SystemTime;
        let mut dt_now: DateTime<chrono::offset::Local>;
        let mut hours: u32;
        let mut mins: u32;
        let mut secs: u32;
        let mut is_pm: bool;
        let mut sleep_dur: Duration;

        loop{
            // Check if we recieved a signal from the calling thread
            match rx.try_recv(){
                Ok(s) => self.recv_sig(s),
                Err(_) => {},
            }

            sys_now = SystemTime::now();
            now = sys_now
                .duration_since(SystemTime::UNIX_EPOCH)
                .expect("Uhh current time before UNIX_EPOCH...")
                .as_secs();

            // get time in useful format
            dt_now = DateTime::<chrono::offset::Local>::from(sys_now);
            is_pm = dt_now.hour12().0;
            hours = dt_now.hour12().1;
            mins = dt_now.minute();
            secs = dt_now.second();


            // output time
            println!("{}:{}:{} {}", hours, mins, secs, if is_pm{"PM"} else{"AM"});

            
            if self.data.lock().unwrap().display_time{
                self.display_time(hours, mins, secs);
            }
            
            
            // Calculate how long to sleep for. 
            sleep_dur = Duration::from_secs(now + 1) - 
                        SystemTime::now()
                            .duration_since(SystemTime::UNIX_EPOCH)
                            .expect("Uhh current time before UNIX_EPOCH...");

            thread::sleep(sleep_dur);
        }
    }


}

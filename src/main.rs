use lazy_static::lazy_static;
extern crate linux_embedded_hal as hal;
extern crate pwm_pca9685 as pca9685;
use pca9685::{ Pca9685, SlaveAddr };
use std::sync::Mutex;
use text_io::read;
use gpio::{GpioIn, GpioOut};
use std::thread;
use std::time::Duration;
mod clock;

lazy_static! {
    static ref PWM_DRIVERS: [Mutex<Pca9685<hal::I2cdev>>; 2] = init_pca_drivers();
}


fn init_pca_drivers() -> [Mutex<Pca9685<hal::I2cdev>>; 2]{

    let i2cdev0 = hal::I2cdev::new("/dev/i2c-1").unwrap();
    let i2cdev1 = hal::I2cdev::new("/dev/i2c-1").unwrap();

    let address0 = SlaveAddr::default();
    let mut pwm0 = Pca9685::new(i2cdev0, address0);
    let (a5, a4, a3, a2, a1, a0) = (false, false, false, false, true, false);
    let address1 = SlaveAddr::Alternative(a5, a4, a3, a2, a1, a0);
    let mut pwm1 = Pca9685::new(i2cdev1, address1);

    pwm0.reset_internal_driver_state();
    pwm1.reset_internal_driver_state();

    // Default osc_clock of 2.5*10^7
    // prescale = round( osc_clock / (4096 * update_rate) ) - 1
    pwm0.set_prescale(3).unwrap();
    pwm1.set_prescale(3).unwrap();

    pwm0.enable().unwrap();
    pwm1.enable().unwrap();
    
    let ret =  [Mutex::new(pwm0), Mutex::new(pwm1)];

    return ret;
}


fn main() {
    let mut hv_enable = gpio::sysfs::SysFsGpioOutput::open(21).unwrap();
    hv_enable.set_value(true).unwrap();

    let clk = clock::Clock::new();

    clk.init_display();
    clk.start_clock_thread();
    hv_enable.set_value(false).unwrap();
    let mut line = String::new();
    let b1 = std::io::stdin().read_line(&mut line).unwrap();
    clk.stop_clock_thread();
    


}

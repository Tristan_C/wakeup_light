#!/usr/bin/python3

# Wakeup_Light
# Copyright (C) 2019  Tristan Carlson
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import serial
import RPi.GPIO as GPIO
import time

"""
Used to comminicate with remote devices.
The base is consitered master and all other devices slaves.  Slaves
do not write to the network unless requested by master.

Each request is a variable length, any valies are assumed little endian,
and is layed out as follows (addressed in bytes):
| 0-1       | 2      | 3-4     | 5-8       | ...  | (n-1) - n |
| 0x01 0x02 | device | command | data len  | data |0x03 0x04  |


"""

REQ_START = b'\x01\x02'
REQ_END = b'\x03\x04'

SER = serial.Serial( '/dev/ttyS0', 2400, timeout=1)

RS_STATE_PIN = 21

# Setup GPIO
GPIO.setmode(GPIO.BCM)
GPIO.setup(RS_STATE_PIN, GPIO.OUT)

print("COM: serial connection on '{}' Baudrate = {}".format(SER.port, SER.baudrate))
print("COM: RS-485 modlue state pin = {} (bcm)".format(RS_STATE_PIN))


class SHARED_CMDS:
    RESET = 0xFFFF
    DISCOVER = 0xFFFE
    GET_VERSION = 0xFFFD


def write(bytestr):
    # TODO Investigate min wait times here
    GPIO.output(RS_STATE_PIN, GPIO.HIGH)
    time.sleep(0.1)
    ret = SER.write(bytestr)
    time.sleep(0.1)
    GPIO.output(RS_STATE_PIN, GPIO.LOW)

    return ret


def readln():
    GPIO.output(RS_STATE_PIN, GPIO.LOW)
    return SER.readline()


def read():
    GPIO.output(RS_STATE_PIN, GPIO.LOW)
    return SER.read()


def send_cmd(dev, cmd, data=b'', timeout=4):
    start = time.time()
    try:
        data = data.encode()
    except AttributeError:
        pass
    data = escape_data(data)
    data_len = len(data).to_bytes(4, byteorder='little')
    dev = dev.to_bytes(1, byteorder='little')
    cmd = cmd.to_bytes(2, byteorder='little')

    while (time.time() - start) <= timeout:
        # Send request
        write(REQ_START + dev + cmd + data_len + data + REQ_END)

        # Get response
        valid = False
        resp = b''
        ind = -1
        while (time.time() - start) < timeout:
            byte = read()
            try:
                resp = bytes([resp])
            except TypeError:
                pass
            resp = resp + byte
            ind += 1

            if ind == 1 and resp != REQ_START:
                if len(resp) > 0:
                    resp = resp[1]
                ind = 0

            if ind >= 10 and resp[index-1:index+1] == REQ_END:
                resp_dta_len = int.from_bytes(resp[5:9], 'little')
                resp_dta_len_actual = len(resp[9:ind -1])

                if resp_dta_len != resp_dta_len_actual:
                    valid = False
                    break

                valid = True
                break

        # Check response for validity
        if not resp or not valid or resp[2] != dev or resp[3:5] != cmd:
            continue
        
        
        return resp

    return None

def parse_resp(resp):
    ret = {}
    ret['device'] = int.from_bytes(resp[2], 'little')
    ret['command'] = int.from_bytes(resp[3:5], 'little')
    data_len = int.from_bytes(resp[5:9], 'little')
    data_start = 9
    data_end = data_start + data_len

    if data_len == 0:
        ret['data'] = b''
        return ret

    ret['data'] = de_escape_data(resp[data_start:data_end])

    return ret

def escape_data(bytestr):
    ret = b''
    for b in bytestr:
        if b in REQ_START or b in REQ_END or b == '\x1B':
            ret = ret + b'\x1B'
        ret = ret + bytes([b])

    return ret

def de_escape_data(bytestr):
    ret = b''
    prev = ''
    for b in bytestr:
        if b == b'\x1B' and prev != b'\x1B':
            continue
        ret = ret + b
        prev = b

    return ret





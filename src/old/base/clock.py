import time
from board import SCL, SDA
import busio
from adafruit_pca9685 import PCA9685
import RPi.GPIO as GPIO

class clock:
    running = False

    def __init__(self, hv_enable_pin, channels_dict=None):
        
        GPIO.setmode(GPIO.BCM)

        if clock.running:
            raise Exception("Clock instantiated more than once!")
        else:
            clock.running = True

        # Map what nixies are on what channel.  chanels < 16 on pca0
        if channels_dict:
            self.channels_dict = channels_dict
        else:
            self.channels_dict = {0:[0,1,2,3], 1:[4,5,6,7], 2:[8,9,10,11], 
                    3:[12,13,14,15], 4:[16,17,18,19], 5:[20,21,22,23]}

        # Init pwm drivers
        i2c_bus = busio.I2C(SCL, SDA)
        self.pca0 = PCA9685(i2c_bus, address=0x40)
        self.pca1 = PCA9685(i2c_bus, address=0x41)
        self.pca0.frequency = 1500
        self.pca1.frequency = 1500

        # set all outputs high (blank the bcd chips).
        self.blank_output()


        # Turn on high volage
        self.hv_pin = hv_enable_pin
        GPIO.setup(hv_enable_pin, GPIO.OUT)
        GPIO.output(hv_enable_pin, 1)

    def blank_output(self):
        for nixie in self.channels_dict:
            self.set_digit(nixie, 'blank')

    def set_digit(self, digit, val):
        if val == 'blank':
            val = 0x1111

        for channel in self.channels_dict[digit]:
            if channel >= 16:
                driver = self.pca1
                channel = channel % 16
            else: 
                driver = self.pca0

            if (val >> x) & 0b01:
                driver.channels[channel].duty_cycle = 0xffff
            else:
                driver.channels[channel].duty_cycle = 0x00



    def display(self, display_list):
        if len(display_list) != 6:
            raise Exception("Invalid display list size. need len 6")

        for digit in range(6):
            self.set_digit(digit, display_list[digit])


    



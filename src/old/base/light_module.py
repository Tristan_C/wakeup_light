#!/usr/bin/python3

# Wakeup_Light
# Copyright (C) 2019  Tristan Carlson
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import COM

"""
Module to control remmote light.
"""
class CMDS(COM.SHARED_CMDS):
    SET_BRIGHTNESS = 0x01

def set_brightness(dev, brightness, timeout=4):
    # Sets brightness on remote light.  brightness directly corresponds to 
    # pwm output value of arduino (0-255)
    if brightness > 255 or brightness < 0:
        return False

    resp = COM.send_cmd(dev, 
                        SET_BRIGHTNESS, 
                        data=brightness.to_bytes(1, byteorder='little'),
                        timeout=timeout)
    
    if resp:
        return True


    return False


def get_version(dev):
    resp = COM.send_cmd(dev, CMDS.GET_VERSION)
    if not resp:
        return None

    resp = COM.parse_resp(resp)

    return resp['data']

def reset(dev):
    resp = COM.send_cmd(dev, CMDS.RESET)
    if not resp:
        return False
    
    return True















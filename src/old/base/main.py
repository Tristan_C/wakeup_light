#!/usr/bin/python3

# Wakeup_Light
# Copyright (C) 2019  Tristan Carlson
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import serial
import RPi.GPIO as GPIO
import time
import COM
import light_module

VERSION = "0.1"



def init():    
    # TODO add parsing of a .cfg file to allow different settings.
    devs = get_devs()

    return devs

def get_devs():
    # Look for devices on the network.
    devs = {}
    for d in range(16):
        print("checking device {}".format(d))
        resp = COM.send_cmd(d, COM.SHARED_CMDS.DISCOVER, data=VERSION.encode())
       
        if not resp:
            continue
        else:
            break

        resp = COM.parse_resp(resp)
        devs[resp['device']] = resp['data'].decode('utf-8')

    
    
    return devs
    

def main():
    devs = init()
    print("Devices found:", devs)

    while True:
        for dev in devs:
            val = int(input("PWM value for", str(dev) + ": "))
            if not light_module.set_brightness(dev, val):
                print("Failed to set brightness for", str(dev))







main()

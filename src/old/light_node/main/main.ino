/* Wakeup_Light
 * Copyright (C) 2019  Tristan Carlson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
 
#include <EEPROM.h>

int ledPin = 5;
int DE_pin = 2;
int RE_pin = 3;
char REQ_START[2] = {0x01, 0x02};
char REQ_END[2] = {0x03, 0x04};
char BUFFER[1024];
uint8_t ID;
char *VERSION = "0.1";


struct CMD{
  long dta_start;
  long dta_len;
  uint16_t cmd;
};


void(* resetFunc) (void) = 0;

/*
 * Listens for a command. If one is found the relevent info is placed in a
 * CMD struct and returned.  This function will block until a valid command is recieved.
 * 
 * CMD.dta_start, CMD.dta_len both index buf (passed to this fn), so the caller is 
 * responsible for preserving buf long enough to ensure returned values are actually valid.
 */

struct CMD listen_for_cmd(char *buf, int buf_len){
  digitalWrite(DE_pin, LOW);
  digitalWrite(RE_pin, LOW);
  
  while(1){
      int i = -1;
      uint8_t valid = 0;
      long *dta_len;
      long dta_len_actual;
      uint8_t *dev;
      uint16_t *cmd;
      long dta_start = -1L;
      
      while (1){
          if (i >= buf_len){
            break;
          }
          buf[i] = Serial.read();
          if (! buf[i]){
            continue;
          }
          i += 1;
          
    
          if (i == 1 && (buf[0] != REQ_START[0] && BUFFER[1] != REQ_START[1])){
            i = 0;
            buf[0] = buf[1];
            continue;
          }

          
    
    
          if (i >= 10 && (buf[i-1]) == REQ_END[0] && buf[i] == REQ_END[1]){
            // Get expected data length
            dta_len = (long*) &buf[5];
            // Calculate actual data length
            dta_len_actual = i - 10 + 1;
            // Grab the ID of the device the comand was intended for
            dev = (uint8_t*) &buf[2];
            // Grab the CMD
            cmd = (uint16_t*) &buf[3];
            Serial.print(*dev);
            
            if (*dta_len == dta_len_actual){
              valid = 1;
              break;
            }
            else{
              valid = 0;
              break;
            }
          }
    
          
      }
        
    
      if (! valid || *dev != ID){
        continue;
      }
      

      if (*dta_len > 0){
        dta_start = 9L;
      }
    
        
      struct CMD ret = {.dta_start=dta_start, .dta_len=*dta_len, .cmd=*cmd};
      return ret;
  }
}

void execute_cmd(struct CMD *cmd){
  return;
  switch(cmd->cmd){
    case 0x01:
      change_brightness(cmd);
      break;
    case 0xFFFF:
      resetFunc();

    case 0xFFFE:
      discover(cmd);
      
  }
  
}


void change_brightness(struct CMD *cmd){
  uint8_t val = (uint8_t) BUFFER[cmd->dta_start];
  analogWrite(ledPin, val);
  respond(cmd->cmd, NULL, 0);
}

void discover(struct CMD *cmd){
  respond(cmd->cmd, VERSION, sizeof(VERSION));
}


void respond(uint16_t cmd, char *data, long data_len){
  digitalWrite(DE_pin, HIGH);
  digitalWrite(RE_pin, HIGH);
  delay(10);
  
  Serial.write(REQ_START, 2);
  Serial.write(ID);
  Serial.write(cmd);
  Serial.write(data_len);
  if (data != NULL){
    Serial.write(data, data_len);
  }
  Serial.write(REQ_END, 2);
  
  delay(10);
  digitalWrite(DE_pin, LOW);
  digitalWrite(RE_pin, LOW);
}


void setup() {
  //EEPROM.get(0, ID);
  ID=1;
  // Setup pins
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);
  pinMode(ledPin, OUTPUT);  
  pinMode(DE_pin, OUTPUT);
  pinMode(RE_pin, OUTPUT);

  // Setup rs485 chip + serial
  Serial.begin(2400);
  Serial.setTimeout(1000);
  digitalWrite(DE_pin, LOW);
  digitalWrite(RE_pin, LOW); 
  digitalWrite(LED_BUILTIN, LOW);

  delay(10);
  
}


void loop() {

  struct CMD c = listen_for_cmd(BUFFER, 1024);

  execute_cmd(&c);  
  
}



/* Wakeup_Light
 * Copyright (C) 2019  Tristan Carlson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */
 
#include <EEPROM.h>
/* Writes unique id to nano */


void setup() {

  uint8_t id = 0x03;
  
  EEPROM.write(0, id);
  Serial.begin(2400);
}

void loop() {
  uint8_t val;
  EEPROM.get(0, val);

  Serial.print("eeprom id = ");
  Serial.print(val);
  Serial.println("");
  delay(2000); 

}

import time
from board import SCL, SDA
import busio
from adafruit_pca9685 import PCA9685
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)

# Turn on 170 v power!
GPIO.setup(21, GPIO.OUT)
GPIO.output(21, 0)
#time.sleep(3)

# Create the I2C bus interface.
i2c_bus = busio.I2C(SCL, SDA)
 
# Create a simple PCA9685 class instance
try:
    pca0 = PCA9685(i2c_bus)
    pca0.frequency = 1000
except:
    print("No pca @ 0x40")
try:
    pca1 = PCA9685(i2c_bus, address=0x42)
    pca1.frequency = 1000
except:
    print("No pca @ 0x42")



def display_num(n):
    for x in range(4):
        pca.channels[x].duty_cycle = 0xFFFF

    for x in range(4):
        if (n >> x) & 0b01:
            pca.channels[x].duty_cycle = 0xffff
        else:
            pca.channels[x].duty_cycle = 0
        
if input("Enter board to use: ") == "0":
    drv = pca0
else:
    drv = pca1

while True:
    d = int(input("enter duty cycle: "))
    ch = int(input("Enter channel: "))
    drv.channels[ch].duty_cycle = d


#for x in range(16):
#    pca.channels[x].duty_cycle = 0xFFFF

#time.sleep(10)


#while True:
    #nput =input("next?")
#    nput = ''
#    for x in range(10):
#        if nput != '':
#            GPIO.output(21, 0)
#            GPIO.cleanup()
#            raise SystemExit
#        display_num(x)
#        time.sleep(0.5)



    
